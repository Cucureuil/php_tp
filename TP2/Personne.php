<?php

/**
 * Created by PhpStorm.
 * User: Jinsei
 * Date: 05/10/2017
 * Time: 08:15
 */
// VERIFIE LES TYPES
declare(strict_types=1);

class Personne {

    /**
     * ATTRIBUTS
     */
    public $nom, $prenom, $age;

    /**
     * Personne constructor.
     * @param String $n String, nom de la personne à initialiser
     * @param String $p String, prenom de la personne à initialiser
     * @param int $a int, age de la personne à initialiser
     */
    public function __construct(String $n,String $p,int $a){
        $this->nom=$n;
        $this->prenom=$p;
        $this->age=$a;
    }

    /**
     * Affiche les informations d'une personne
     */
    public function toString() : String {
        $s  = "<div id='presentation'>";
        //$s .= "<img src=\"./img/AH.jpg\" alt=\"Beau Paysage de printemps\" style=\"width:304px;height:auto;\">";
        $s .= "<div id='info'>";
        $s .= "<p>Identité : $this->nom $this->prenom</p>";
        $s .= "<p>Âge : $this->age</p>";
        $s .= "</div></div>";
        return $s;
    }
}