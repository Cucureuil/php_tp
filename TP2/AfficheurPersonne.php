<?php

/**
 * Created by PhpStorm.
 * User: Jinsei
 * Date: 05/10/2017
 * Time: 09:28
 */
// IMPORT
require_once "Personne.php";

class AfficheurPersonne
{
    /**
     * ATTRIBUTS
     */
    public $personne;

    /**
     * AfficheurPersonne constructor.
     * @param Personne $p , la personne a afficher
     */
    public function __construct(Personne $p){
        if($p!=null){
            $this->personne=$p;
        }
    }

    /**
     * Affiche peu de détails sur une personne
     * @return String les caractéristiques
     */
    public function vueCourte() : String {
        $nom=($this->personne)->nom;
            $prenom=$this->personne->prenom;

        $s="<p>$prenom $nom</p>";
        $s.="<p></p>";
        return $s;
    }
}